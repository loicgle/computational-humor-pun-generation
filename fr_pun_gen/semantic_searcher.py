import pandas as pd
import fasttext
import fasttext.util
import numpy as np
import math
import colorama
from colorama import Fore


def get_farthest(input_list,initial_word,ft):
    
    word_list = []
    for e in input_list:
        word_list.append(e['word'])

    word_vec = ft.get_word_vector(initial_word)


    def vec_cos_angle(vector_1, vector_2):
        unit_vector_1 = vector_1 / np.linalg.norm(vector_1)

        unit_vector_2 = vector_2 / np.linalg.norm(vector_2)

        dot_product = np.dot(unit_vector_1, unit_vector_2)

        return math.cos(np.arccos(dot_product))

    cur_similarity = 1
    cur_word = ""

    for w in word_list:
        vec = ft.get_word_vector(w)
        angle = vec_cos_angle(word_vec,vec)
        if(angle<cur_similarity):
            cur_similarity=angle
            cur_word = w

    return cur_word

def get_closest(input_list,initial_word,ft):
    word_vec = ft.get_word_vector(initial_word)

    def vec_cos_angle(vector_1, vector_2):
        unit_vector_1 = vector_1 / np.linalg.norm(vector_1)

        unit_vector_2 = vector_2 / np.linalg.norm(vector_2)

        dot_product = np.dot(unit_vector_1, unit_vector_2)

        return math.cos(np.arccos(dot_product))

    cur_similarity = -1
    cur_word = ""

    for w in input_list:
        if(w.lower() == initial_word.lower()):
            continue
        vec = ft.get_word_vector(w)
        angle = vec_cos_angle(word_vec,vec)
        if(angle>cur_similarity and angle < 0.98):
            cur_similarity=angle
            cur_word = w

    return cur_word