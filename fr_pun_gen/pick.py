from colorama import Fore


def isAmbigu(word,lex): # => This method look if a word is ambiguous or not, based on our criteria
    list_pos = []   # a list for parts of speech (pos)
    for i in lex.index: 
        if(lex['ortho'][i] == word): # for the given word
            list_pos.append(lex['cgram'][i]) # add its pos in the list
    if(len(list_pos) > 1):  # if the word have more than one possible pos 
                            # (we do not analyze the sentence to know what 
                            # the current position is, so the word is qualified as ambiguous)
        return True
    else:
        return False

def getLast(pretenders_list): # => This method take the list of predenters and return the closest
                                # word to the end of the sentence
    max_index = 0
    last_word = 0
    for i in range(len(pretenders_list)): # for each word
        if(pretenders_list[i]['location'] > max_index): # simple comparison
            max_index = pretenders_list[i]['location']
            last_word = i
    return pretenders_list[i]


def pick(word_list,lex):
    pretenders_list=[]
    for i in lex.index:
        for word in word_list: # We start by taking a look at all words
            if(lex['ortho'][i] == word):
                pos = lex['cgram'][i]
                if((pos == 'NOM' or pos == 'ADJ') & (isAmbigu(word,lex) == False) & (lex['nbhomoph'][i] > 2)):  # to check their POS, ambiguousness and nb of homophones
                    pretenders_list.append({"word" : word,"lemme":lex['lemme'][i],"phon":lex['phon'][i],"cgram":pos})   # the word is added in the list 
                    
    if(len(pretenders_list) == 0): 
        print(Fore.RED + "This sentence has no pretending word for a pun according to our criteria !")
        print(Fore.RESET)
        return {}


    for i in range(len(pretenders_list)): # each word location is addded in the list
        loc = {"location" : word_list.index(pretenders_list[i]["word"])}
        pretenders_list[i].update(loc)

    pretender_word = getLast(pretenders_list) # the best word is always le end-closer-word
    
    return pretender_word