from colorama import Fore
import semantic_searcher

def getPhonList(word,lex): # => this method take the selected word and return the list with all its homophones
    freq = 0
    phon_list = []

    for j in lex.index:
        if(lex['phon'][j] == word['phon']):             # if a word have the save phonetic form than our word (homophone)
            flag1 = lex['lemme'][j] != word['lemme']    # 1st flag : Lemmatized form must be different
            flag2 = lex['cgram'][j] == word['cgram']    # 2nd flag : POS must be the same
            freq = (int(lex['freqfilms2'][j]) + int(lex['freqlivres'][j])) / 2  # we also need the frequency (here is an average of both frequency fields)
            flag3 = freq > 2                            # 3rd flag : The frequency must be sufficient    
            if(flag1 & flag2 & flag3):  # each flags have to be checked
                phon_list.append({"word" : lex['ortho'][j],
                                    "phon" : lex['phon'][j],
                                    "cgram" : lex['cgram'][j]}) # The homophone is add into the list
    return phon_list

def swap(pretender_word,lex,sem_model):
    phon_list = getPhonList(pretender_word,lex)      #Geting the list of homophones
    if(len(phon_list) == 0):
        print(Fore.RED + "There is no homophone avalible for pun")
        print(Fore.RESET)
        return ""
    else:
        farthest_homophone = semantic_searcher.get_farthest(phon_list,pretender_word['word'],sem_model)     #Getting the semanticaly farthest word
        return farthest_homophone
