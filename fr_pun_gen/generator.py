import pandas as pd
import re

from colorama import Fore

import fasttext.util

import semantic_searcher
import topic_j1
import subject_j1
import build_sentence_j1
from pick import pick
from swap import swap


import os
clear = lambda: os.system('clear')
clear()     ## Clear the terminal before execution

import json
with open('fr_pun_gen/setup.json') as json_file:
    setup = json.load(json_file)
YOUR_API_KEY = setup["API_KEY"]
PATH_TO_DATASET = setup["path_to_dataset"]
PATH_TO_OUTPUT = setup["path_to_output"]
CHOICE = setup["choice"]

def sentence_input():
    return input("Enter a sentence : ")

def df_input():
    input_df = pd.read_json(PATH_TO_DATASET)
    print(input_df)
    print()
    return input_df

def load_ft():
    ft = fasttext.load_model('vecfr/cc.fr.300.bin')
    return ft

def load_lex():
    lex = pd.read_csv('Lexique383/Lexique383.tsv', sep='\t')    # import the french lexicon "Lexique 3"
    return lex

def start(choice,sem_model, lex):
    
    mydf=pd.DataFrame(columns=['text'], index=[0])
    
    if(choice == 2):
        input_df = df_input()
        mydf=input_df
    elif(choice == 1):
        input_sentence = sentence_input()
        mydf['text'][0]=input_sentence
        
    res_list = []
    
    for text_id in mydf.index:
        input_sentence = mydf['text'][text_id]
        nopunc_sentence = re.sub("[^'^\w\s]", "", input_sentence)   # remove punctuation
        
        word_list = nopunc_sentence.split()                         # split the sentence into a word list
        
        print(Fore.BLUE + input_sentence)
        
        pretender_word = pick(word_list,lex)
        if(pretender_word == {}):
            continue
        
        farthest_homophone = swap(pretender_word,lex,sem_model)
        if(farthest_homophone == ""):
            continue

        new_sentence = {'sentence' : input_sentence, 'pick_word' : pretender_word['word'], 'swap_word' : farthest_homophone}
        
        new_sentence = subject_j1.get_subject(new_sentence,YOUR_API_KEY)
        
        new_sentence = topic_j1.topic(new_sentence,YOUR_API_KEY)
        
        new_sentence['topic'] = semantic_searcher.get_closest(new_sentence['topic'],new_sentence['swap_word'],sem_model)
        
        new_sentence = build_sentence_j1.rebuild(new_sentence,YOUR_API_KEY)
        
        print()
        print(Fore.BLACK + "Sentence n°" + str(text_id))
        print(Fore.RED + str(new_sentence))
        print(Fore.RESET)
        
        res_list.append(new_sentence)
        df = pd.DataFrame(res_list)
        
        df.to_json(PATH_TO_OUTPUT)

    
def main():

    
    if(CHOICE == 1 or CHOICE == 2):
        sem_model = load_ft()
        lex=load_lex()
        start(CHOICE,sem_model,lex)
    else:
        print("Error : wrong choice value")
    
        
if __name__ == "__main__":
    main()
