import requests

def rebuild(sentence,YOUR_API_KEY):
    res = requests.post("https://api.ai21.com/studio/v1/j1-jumbo/complete",
        headers={"Authorization": "Bearer " + YOUR_API_KEY},
        json={
            "prompt": "Changer le sujet de la phrase initiale avec le nouveau sujet pour créer une nouvelle phrase.\n--\nPhrase: L'homme est tombé, il a cassé un verre.\nSujet: l'homme\nNouveau sujet: le poète\nNouvelle phrase: Le poète est tombé, il a cassé un verre.\n--\nPhrase: Il a dû se confiner, il a pris du poids.\nSujet: il\nNouveau sujet: le maraicher\nNouvelle phrase: Le maraicher a dû se confiner, il a pris du poids.\n--\nPhrase: Tu devrais faire attention à ce que tu manges, ce n'est pas très sain.\nSujet: tu\nNouveau sujet: le prêtre\nNouvelle phrase: le prêtre devrait faire attention à ce qu'il mange, ce n'est pas très sain.\n--\nPhrase: Après la fête, il a passé le balai.\nSujet: il\nNouveau sujet: le danseur\nNouvelle phrase: Après la fête, le danseur a passé le balai.\n--\nPhrase: Pour l'anniversaire du voisin, il y avait un gâteau aux amandes.\nSujet: voisin\nNouveau sujet: le policier\nNouvelle phrase: Pour l'anniversaire du policier, il y avait un gâteau aux amandes.\n--\nPhrase: "+sentence['sentence']+"\nSujet: "+sentence['subject']+"\nNouveau sujet: "+sentence['topic']+"\nNouvelle phrase:",
            "numResults": 1,
            "maxTokens": 50,
            "temperature": 0.8,
            "topKReturn": 0,
            "topP":0.98,
            "countPenalty": {
                "scale": 0,
                "applyToNumbers": False,
                "applyToPunctuations": False,
                "applyToStopwords": False,
                "applyToWhitespaces": False,
                "applyToEmojis": False
            },
            "frequencyPenalty": {
                "scale": 0,
                "applyToNumbers": False,
                "applyToPunctuations": False,
                "applyToStopwords": False,
                "applyToWhitespaces": False,
                "applyToEmojis": False
            },
            "presencePenalty": {
                "scale": 0,
                "applyToNumbers": False,
                "applyToPunctuations": False,
                "applyToStopwords": False,
                "applyToWhitespaces": False,
                "applyToEmojis": False
        },
        "stopSequences":["--"]
        }
    )
    
    if(res.status_code==429):
        print("ALERT : Your API key has reach the limit request/token number !")
        exit()
    data = res.json()
    temp = data['completions'][0]['data']['text']
    temp = temp.replace("\n","")
    sentence['rebuilt_sentence'] = temp
    return sentence