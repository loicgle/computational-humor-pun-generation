import pandas as pd
import requests
from transformers import DataCollator


def topic(sentence_df,YOUR_API_KEY):
    
    topics_list = []
    topics = requests.post("https://api.ai21.com/studio/v1/j1-jumbo/complete",
        headers={"Authorization": "Bearer "+YOUR_API_KEY},
        json={
            "prompt": "Changez le contexte de la phrase selon l'homonyme remplaçant.\n--\nphrase: L'homme est tombé, il a cassé un verre.\nmot à remplacer: verre\nhomonyme: vers\nnouveau contexte de l'homonyme remplaçant: Le poète\n--\nphrase: Il a du se confiné, il a pris du poids.\nmot à remplacer: poids\nhomonyme: pois\nnouveau contexte de l'homonyme remplaçant: Le maraicher\n--\nphrase: Tu devrais faire attention à ce que tu mange, ce n'est pas très sain.\nmot à remplacer: sain\nhomonyme: saint\nnouveau contexte de l'homonyme remplaçant: le pape\n--\nphrase: Après la fête, il a passé le balai.\nmot à remplacer: balai\nhomonyme: ballet\nnouveau contexte de l'homonyme remplaçant: le danseur\n--\nphrase: Pour l'anniversaire du voisin, il y avait un gateau aux amandes.\nmot à remplacer: amandes\nhomonyme: amendes\nnouveau contexte de l'homonyme remplaçant: policier\n--\nphrase: "+sentence_df['sentence']+"\nmot à remplacer: "+sentence_df['pick_word']+"\nhomonyme: "+sentence_df['swap_word']+"\nnouveau contexte de l'homonyme remplaçant:",
            "numResults": 15,
            "maxTokens": 15,
            "temperature": 0.7,
            "topKReturn": 0,
            "topP":1,
            "countPenalty": {
                "scale": 0,
                "applyToNumbers": False,
                "applyToPunctuations": False,
                "applyToStopwords": False,
                "applyToWhitespaces": False,
                "applyToEmojis": False
            },
            "frequencyPenalty": {
                "scale": 0,
                "applyToNumbers": False,
                "applyToPunctuations": False,
                "applyToStopwords": False,
                "applyToWhitespaces": False,
                "applyToEmojis": False
            },
            "presencePenalty": {
                "scale": 0,
                "applyToNumbers": False,
                "applyToPunctuations": False,
                "applyToStopwords": False,
                "applyToWhitespaces": False,
                "applyToEmojis": False
        },
        "stopSequences":["--"]
        }
    )
    if(topics.status_code==429):
        print("ALERT : Your API key has reach the limit request/token number !")
        exit()
    data = topics.json()
    for i in range(15):
        temp = data['completions'][i]['data']['text']
        temp = temp.replace("\n","")
        topics_list.append(temp)
    topics_list = list(dict.fromkeys(topics_list))
    sentence_df['topic'] = topics_list
    return sentence_df
