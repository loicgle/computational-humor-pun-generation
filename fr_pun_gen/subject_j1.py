import requests

def get_subject(sentence,YOUR_API_KEY):
    res = requests.post("https://api.ai21.com/studio/v1/j1-grande/complete",
        headers={"Authorization": "Bearer "+YOUR_API_KEY},
        json={
            "prompt": "Repérer le sujet de la phrase\n--\nPhrase: l'homme est tombé, il a cassé un verre\nSujet: homme\n--\nPhrase: il a dû se confiner, il a pris du poids\nSujet: il\n--\nPhrase: tu devrais faire attention à ce que tu manges, ce n'est pas très sain\nSujet: tu\n--\nPhrase: le français est la dixième langue maternelle la plus parlée au monde\nSujet: français\n--\nPhrase: après la fête, j'ai passé le balai\nSujet: je\n--\nPhrase: pour l'anniversaire du voisin, il y avait un gâteau aux amandes\nSujet: voisin\n--\nPhrase: "+sentence['sentence']+"\nSujet:",
            "numResults": 1,
            "maxTokens": 20,
            "temperature": 0.8,
            "topKReturn": 0,
            "topP":0.98,
            "countPenalty": {
                "scale": 0,
                "applyToNumbers": False,
                "applyToPunctuations": False,
                "applyToStopwords": False,
                "applyToWhitespaces": False,
                "applyToEmojis": False
            },
            "frequencyPenalty": {
                "scale": 0,
                "applyToNumbers": False,
                "applyToPunctuations": False,
                "applyToStopwords": False,
                "applyToWhitespaces": False,
                "applyToEmojis": False
            },
            "presencePenalty": {
                "scale": 0,
                "applyToNumbers": False,
                "applyToPunctuations": False,
                "applyToStopwords": False,
                "applyToWhitespaces": False,
                "applyToEmojis": False
          },
          "stopSequences":["--"]
        }
    )
    
    if(res.status_code==429):
        print("ALERT : Your API key has reach the limit request/token number !")
        exit()
    data = res.json()
    temp = data['completions'][0]['data']['text']
    temp = temp.replace("\n","")
    sentence['subject'] = temp
    return sentence