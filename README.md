# Pun Generation

This program generates puns from sentences.

Here is the construction pipeline of a word game that we propose:
- (0) We give a sentence / or a set of sentences
- (1) Choice of the word on which to apply the pun.
- (2) Choice of a homophone (if there is one), the most semantically distant from the word chosen in (1)
- (3) Jurassic is asked to offer 15 new subjects, related to the homophone from (2)
- (4) In the list from (3) (and having removed the duplicates) we choose the new subject of the sentence, the closest semantically to the homophone from (2)
- (5) Jurassic is asked to locate the subject of the sentence from (0)
- (6) Jurassic is asked to reconstruct the sentence (0) by replacing the subject from (5) with the one from (4)

Before any use, modules have to be installed.

## 1. External modules

### Libraries

This program has been developed with Python 3.8.10 and should work with any version similar or newer.

From the command line, install the required Python libraries:

1. Create a virtual environment to limit the libraries scope:

       python -m venv env_pun

2. Activate the virtual environment:

       source env_pun/bin/activate

3. Install the libraries:

       python -m pip install -r requirements.txt

### Fasttext [FR]

This program uses the French model of fasttext. you have to download it here: https://fasttext.cc/docs/en/crawl-vectors.html

Scroll the page until the "French" row. You'll need the "bin" file. 

Once this is done, make sure that the "vecfr" folder is at the root of the project (see file tree diagram)

### Lexique 3

This program uses "Lexique 3", download it here: http://www.lexique.org/

Once downloaded, make sure that the Lexique383 folder is at the root of the project (see file tree diagram)

### File Tree Diagram
```
>project_root
  >pun_genV1
    >generator.py
    >semantic_searcher.py
    >topic_j1.py
    >build_sentence_j1.py
    >subject_j1.py
    >setup.json
    >pick.py
    >swap.py
  >vecfr
    >cc.fr.300.bin
  >Lexique383
    >Lexique383.tsv
```
Please, make sure to respect this tree. Also, note that every path in the code is a relative path. Your *project_root* can be anywhere.

## Setup

In ***pun_genV1/setup.json*** you have to give some information to the program.

- ***API_KEY***: In this field, you have to give your *AI21studio* API key. First, make sure to create an account  (here : https://studio.ai21.com/) if you don't have one. Then go to the top right of your screen and click on the **profile logo -> Account**. Now you can copy your API key and put it in *setup.json*.

- ***path_to_data***: This field is only used when you want to give a dataset as input. Give the path to the file. The file must be a JSON one, with a ***'text'*** field. The column name is fixed and must be ***'text'***.

- ***path_to_output***: This field is always needed because even with one sentence, the program will create an output *JSON* file. Make sure to indicate a path to the location you want. **The path must contain the file name !**

- ***choice***: This is your choice between only sentence or a whole dataset.

## Program Run

When you execute ***generator*** *(that is the main script)* depending on your *choice* (in the setup file), there is 2 possibilities :

- **1**: You want to run the program with only one sentence that you will type in the terminal

- **2**: You have a data set, and you are ready to run the program on the whole data set. *(obviously, paths have been set up)*

- **Other characters**: Wrong entry, the program end.

Note that your API key is limited in the number of requests you are authorized in, and also the number of tokens generated.

However, don't panic, if you reach the limit in the middle of your build, each result is dynamically added to the output file. The generation will stop, but you won't lose your results or your time.

### Sentences format

Our pipeline uses Jurassic 1, a language model, this model is trained with some precise examples. So if an input sentence is structurally too far from the examples, the results should be bad. On the opposite, if the input sentence format is close to our examples, the results have a better chance to be good. Here are some examples we used to train Jurassic :

- *L'homme est tombé, il a cassé un verre.*
- *Il a du se confiné, il a pris du poids.*
- *Tu devrais faire attention à ce que tu mange, ce n'est pas très sain.*
- *Après la fête, il a passé le balai.*
- *Pour l'anniversaire du voisin, il y avait un gateau aux amandes.*

It is not recommended to use these sentences in the generator. However, you may notice a repeating format: 
- *[subject] [situation] "comma" [action {which ends with a word with homophones} ]*

Here is a sentence example that you can try and that perfectly matches the best format.

- *Le fils a dit au revoir à sa mère.*

**Now you are ready to use the French pun generator !** 

### The output file

The output file must be in the *JSON* format, which reduces the readability of the results. First, there are the fields you will find in it :

- The initial ***sentence***.
- The ***pick_word***, is the word chosen to be changed in a pun.
- The ***swap_word***, is the homophone selected.
- The ***subject*** of the initial sentence.
- The ***topic*** that replaces the subject.
- The ***rebuilt_sentence*** is your pun.

For a better reading, here is a little script that changes the **JSON** to **markdown** thanks to *Pandas*.

```py
import pandas as pd

df = pd.read_json("PATH/TO/OUTPUT/FILE.json")

df.to_markdown("PATH/TO/MD/FILE.md")

```
